package com.example.weatherapp;

/**
 * Created by rabin on 5/23/2015.
 */
public class AppText {

    public static final String NO_WEATHER_DATA = "Sorry, no weather data found.";

    public static final String WEATHER_CITY_COUNTRY = "http://api.openweathermap.org/data/2.5/weather?q=%s,%s&units=metric";
    public static final String WEATHER_LAT_LON = "http://api.openweathermap.org/data/2.5/weather?lat=28.21&lon=84";
    public static final String WEATHER_SEARCH_CITY = "http://api.openweathermap.org/data/2.5/find?type=like&q=pokh&cnt=5";
    public static final String WEATHER_SEARCH_LAT_LON = "http://api.openweathermap.org/data/2.5/find?lat=28.21&lon=84";
}
