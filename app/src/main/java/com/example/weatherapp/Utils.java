package com.example.weatherapp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by samir on 5/21/2015.
 */
public class Utils {

    /**
     * Method for checking if the mobile is connected to internet or not
     *
     * @param context Activity/context from where internet status is being checked
     * @return true if connected to internet false otherwise
     */
    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
