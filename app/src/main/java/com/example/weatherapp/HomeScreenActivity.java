package com.example.weatherapp;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.weatherapp.model.Climate;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.text.DateFormat;
import java.util.Date;


public class HomeScreenActivity extends ActionBarActivity {

    private TextView textViewCityField;
    private TextView textViewUpdatedField;
    private TextView textViewDetailsField;
    private TextView textViewCurrentTemperatureField;
    private TextView textViewWeatherIcon;

    private Typeface weatherFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        textViewCityField = (TextView) findViewById(R.id.textview_city_field);
        textViewUpdatedField = (TextView) findViewById(R.id.textview_updated_field);
        textViewDetailsField = (TextView) findViewById(R.id.textview_details_field);
        textViewCurrentTemperatureField = (TextView) findViewById(R.id.textview_current_temperature_field);
        textViewWeatherIcon = (TextView) findViewById(R.id.textview_weather_icon);

        weatherFont = Typeface.createFromAsset(getAssets(), "fonts/weather.ttf");
        textViewWeatherIcon.setTypeface(weatherFont);

        fetchWeatherFromServer();
    }

    private void fetchWeatherFromServer() {
        WeatherFetcherAsyncTask weatherFetcherAsyncTask = new WeatherFetcherAsyncTask(this);
        weatherFetcherAsyncTask.execute(String.format(AppText.WEATHER_CITY_COUNTRY, "pokhara", "nepal"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void parseJsonResponse(String responseFromServer) {
        if (TextUtils.isEmpty(responseFromServer)) {
            Toast.makeText(this, AppText.NO_WEATHER_DATA, Toast.LENGTH_LONG).show();
        } else {
            try {
                Gson gson = new Gson();
                Climate climate = gson.fromJson(responseFromServer, Climate.class);

                // 200 sent by server if successful
                if (climate.getCod() == 200) {
                    textViewCityField.setText(climate.getCity() + ", " + climate.getSysData().getCountry());
                    textViewDetailsField.setText(climate.getWeatherList().get(0).getDescription()
                            + "\nHumidity: " + climate.getMainData().getHumidity() + "%"
                            + "\nPressure: " + climate.getMainData().getPressure() + " hPa");
                    textViewCurrentTemperatureField.setText(
                            String.format("%.2f", climate.getMainData().getTemperature()) + " ℃");
                    DateFormat dateFormat = DateFormat.getDateTimeInstance();
                    String updatedOn = dateFormat.format(new Date(climate.getUpdatedOn() * 1000));
                    textViewUpdatedField.setText("Last update: " + updatedOn);

                    setWeatherIcon(climate.getWeatherList().get(0).getId(),
                            climate.getSysData().getSunriseTime() * 1000,
                            climate.getSysData().getSunsetTime() * 1000);
                } else {
                    Toast.makeText(this, AppText.NO_WEATHER_DATA, Toast.LENGTH_LONG).show();
                }
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Set the weather icon
     * @param actualId
     * @param sunriseTime
     * @param sunsetTime
     */
    private void setWeatherIcon(int actualId, long sunriseTime, long sunsetTime) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunriseTime && currentTime < sunsetTime) {
                icon = getString(R.string.weather_sunny);
            } else {
                icon = getString(R.string.weather_clear_night);
            }
        } else {
            switch (id) {
                case 2:
                    icon = getString(R.string.weather_thunder);
                    break;
                case 3:
                    icon = getString(R.string.weather_drizzle);
                    break;
                case 7:
                    icon = getString(R.string.weather_foggy);
                    break;
                case 8:
                    icon = getString(R.string.weather_cloudy);
                    break;
                case 6:
                    icon = getString(R.string.weather_snowy);
                    break;
                case 5:
                    icon = getString(R.string.weather_rainy);
                    break;
            }
        }
        textViewWeatherIcon.setText(icon);
    }
}
