package com.example.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * Weather Class holding information about description about weather, icon for current weather, etc
 */
public class Weather {

    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private int id;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
