package com.example.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * SysData class holding country
 */
public class SysData {

    @SerializedName("country")
    private String country;
    @SerializedName("sunrise")
    private long sunriseTime;
    @SerializedName("sunset")
    private long sunsetTime;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public long getSunriseTime() {
        return sunriseTime;
    }

    public void setSunriseTime(long sunriseTime) {
        this.sunriseTime = sunriseTime;
    }

    public long getSunsetTime() {
        return sunsetTime;
    }

    public void setSunsetTime(long sunsetTime) {
        this.sunsetTime = sunsetTime;
    }
}
