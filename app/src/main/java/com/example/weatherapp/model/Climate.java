package com.example.weatherapp.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * The mainData Object for holding weather
 */
public class Climate {

    @SerializedName("sys")
    private SysData sysData;
    @SerializedName("weather")
    private List<Weather> weatherList;
    @SerializedName("main")
    private MainData mainData;
    @SerializedName("dt")
    private long updatedOn;
    @SerializedName("name")
    private String city;
    @SerializedName("cod")
    private int cod;

    public SysData getSysData() {
        return sysData;
    }

    public void setSysData(SysData sysData) {
        this.sysData = sysData;
    }

    public List<Weather> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<Weather> weatherList) {
        this.weatherList = weatherList;
    }

    public MainData getMainData() {
        return mainData;
    }

    public void setMainData(MainData mainData) {
        this.mainData = mainData;
    }

    public long getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(long updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }
}
