package com.example.weatherapp.model;

import com.google.gson.annotations.SerializedName;

/**
 * MainData Class holding detail weather information(humidity, pressure, temperature
 */
public class MainData {

    @SerializedName("temp")
    private double temperature;
    @SerializedName("pressure")
    private String pressure;
    @SerializedName("humidity")
    private String  humidity;

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}
