package com.example.weatherapp;

import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

/**
 * AsyncTask for fetching the weather data from the server
 */
public class WeatherFetcherAsyncTask extends AsyncTask<String, Void, String> {

    private HomeScreenActivity activity;

    WeatherFetcherAsyncTask(HomeScreenActivity activity) {
        this.activity = activity;
    }
    @Override
    protected String doInBackground(String... urls) {
        // get the url sent from asyncTask.execute(url)
        String url = urls[0];
        // a variable to store the server response
        String responseFromServer;
        try {
            // A HttpClient for contacting remote server for data
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity;
            // A HttpResponse object to store the resonse from the HttpClient
            HttpResponse httpResponse;
            // Type of method(Get, Post, Put, Delete)
            // and the url for server communication passed as parameter during initialization
            HttpGet httpGet = new HttpGet(url);

            // get the server response
            httpResponse = httpClient.execute(httpGet);

            // extract the message as String and store as response message from server
            httpEntity = httpResponse.getEntity();
            responseFromServer = EntityUtils.toString(httpEntity);
        } catch (IOException e) {
            e.printStackTrace();
            responseFromServer = null;
        }
        return responseFromServer;
    }

    @Override
    protected void onPostExecute(String responseFromServer) {
        // receives server response returned by the method doInBackground
        // send that message back to the activity
        // As AsyncTask runs in background asynchronously, activity may or may not be in the main
        // thread when data fetching process is completed. So it is better to check if the activity
        // exists and has not exited before sending back data to it
        if (activity != null && !activity.isFinishing()) {
            activity.parseJsonResponse(responseFromServer);
        }
    }


}
